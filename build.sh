#!/bin/bash
# Unofficial bash mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

yarn install --frozen-lockfile
mkdir -p public/

node ./index.js | tee public/output.json | node_modules/.bin/mustache - template.mustache > public/index.html

echo "Successfully ran our build."
