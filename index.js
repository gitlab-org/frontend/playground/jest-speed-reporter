#!/usr/bin/env node
import fs from "node:fs";
import path from 'node:path'
import Decimal from "decimal.js";
import parser from "fast-xml-parser";
import { GitLabAPI, GitLabPagedAPIIterator } from "./lib/api.js";

Decimal.prototype.toJSON = function () {
  return this.toDecimalPlaces(3).toString();
};

const rootDir = import.meta.dirname;
const cacheDir = path.join(rootDir, 'cache');

fs.mkdirSync(cacheDir, {recursive: true})

const projectId = 278964;

async function pullArtifact(job) {
  console.warn(`Get jest junit artifact for '${job.name}': ${job.web_url}`);
  const { data: xml } = await GitLabAPI.get(
    `/projects/${projectId}/jobs/${job.id}/artifacts/junit_jest.xml`
  );

  return { ...job, junit: parseJunitAndExtract(xml) };
}

const noJestJobFile = path.join(cacheDir, 'no_jest_jobs');

const jobNameRegex = /^jest(-with-fixtures)? \d+\/\d+$/;

async function getLastNPipelinesWithJestJobs(n = 50) {
  const pipelinesIterator = new GitLabPagedAPIIterator(
    GitLabAPI,
    `/projects/${projectId}/pipelines`,
    {
      ref: "master",
      // updated_before: '2024-04-16T16:22:28.807Z'
    }
  );

  let noJestJobs = [];

  try {
    noJestJobs = JSON.parse(fs.readFileSync(noJestJobFile, "utf-8"));
  } catch {}

  const results = [];

  for await (const pipeline of pipelinesIterator) {
    if (!["failed", "success"].includes(pipeline.status)) {
      continue;
    }
    if (noJestJobs.includes(pipeline.id)) {
      continue;
    }
    if (results.length >= n) {
      return results;
    }

    const cacheFile = path.join(cacheDir, `${pipeline.id}.json`);

    try {
      results.push(JSON.parse(fs.readFileSync(cacheFile, "utf-8")));
      console.warn("read from cache");
      continue;
    } catch {}

    const jobIterator = new GitLabPagedAPIIterator(
      GitLabAPI,
      `/projects/${projectId}/pipelines/${pipeline.id}/jobs`
    );

    console.warn(`Got data for pipeline: ${pipeline.id}`);

    const jestJobs = [];

    for await (const job of jobIterator) {
      if (!["failed", "success"].includes(job.status)) {
        continue;
      }
      if (jobNameRegex.test(job.name)) {
        jestJobs.push(job);
      }
    }
    if (jestJobs.length) {
      console.warn(`Success! Found some jest jobs for ${pipeline.id} `);
      const res = {
        pipeline,
        jestJobs: await Promise.all(jestJobs.map((job) => pullArtifact(job))),
      };
      results.push(res);
      try {
        fs.writeFileSync(cacheFile, JSON.stringify(res), "utf-8");
      } catch {}
    } else {
      noJestJobs.push(pipeline.id);
      try {
        fs.writeFileSync(noJestJobFile, JSON.stringify(noJestJobs), "utf-8");
      } catch {}
    }
  }
}

function parseJunitAndExtract(xml) {
  const obj = parser.parse(xml, {
    arrayMode: true,
    attributeNamePrefix: "",
    parseNodeValue: false,
    ignoreAttributes: false,
  });
  const suite = obj.testsuites[0].testsuite;

  suite.testcase = flatAndMapCases(suite.testcase);
  return suite;
}

function getTestSuites(jestJobs) {
  return jestJobs
    .flatMap((job) => job.junit)
    .map((suite) => {
      const { name, tests: testsArg, testcase } = suite;
      const tests = Number(testsArg);

      const time = testcase.reduce(
        (sum, current) => Decimal.sum(sum, current.time),
        0
      );

      return {
        name,
        time,
        tests,
        file: testcase[0]?.file,
        average: Decimal.div(time, testcase.length),
        slowest: testcase[0],
      };
    })
    .sort((b, a) => a.average.cmp(b.average));
}

function flatAndMapCases(testcases) {
  if (!testcases) {
    return [];
  }
  if (!Array.isArray(testcases)) {
    return {
      name: testcases.name,
      time: testcases.time,
      file: testcases.file,
    };
  }
  return testcases.flatMap((x) => flatAndMapCases(x));
}

function getTests(jestJobs) {
  return jestJobs
    .flatMap((job) => job.junit)
    .flatMap((suite) => suite.testcase)
    .map((test) => {
      test.time = new Decimal(test.time);
      return test;
    });
}

function calculateStandardDeviation(numbers) {
  // Step 1: Calculate the mean
  const mean = numbers
    .reduce((acc, val) => Decimal.sum(acc, val), 0)
    .div(numbers.length);

  // Step 2: Calculate the squared differences
  const squaredDifferences = numbers.map((num) =>
    Decimal.sub(num, mean).pow(2)
  );

  // Step 3: Calculate the mean of squared differences
  const meanOfSquaredDiff = squaredDifferences
    .reduce((acc, val) => Decimal.sum(acc, val), 0)
    .div(numbers.length);

  // Step 4: Take the square root
  const standardDeviation = meanOfSquaredDiff.sqrt();

  return standardDeviation;
}

async function main() {
  const pipelines = await getLastNPipelinesWithJestJobs();

  const pipelinesParsed = pipelines.map(({ jestJobs, ...rest }) => {
    return {
      ...rest,
      testSuites: getTestSuites(jestJobs),
      slowestTests: getTests(jestJobs),
    };
  });

  const { pipeline, testSuites, slowestTests } = pipelinesParsed[0];

  const allSuites = pipelinesParsed
    .flatMap((x) => x.testSuites)
    .reduce((acc, testSuite) => {
      const { name, tests, time, average, slowest, file } = testSuite;

      acc[name] ||= {
        name,
        tests,
        time: [],
        averages: [],
        file,
      };

      acc[name].time.push(time);
      acc[name].averages.push(average);

      return acc;
    }, {});

  const testSuitesOverTime = Object.values(allSuites)
    .map((suite) => {
      return {
        ...suite,
        minAvg: Decimal.min(...suite.averages),
        medAvg: suite.averages.sort((a, b) => a.cmp(a, b))[
          Math.round(suite.averages.length / 2)
        ],
        maxAvg: Decimal.max(...suite.averages),
      };
    })
    .sort((x, y) => y.medAvg.cmp(x.medAvg));

  const allTests = pipelinesParsed
    .flatMap((x) => x.slowestTests)
    .reduce((acc, test) => {
      const { name, time, file } = test;

      acc[name] ||= {
        name,
        time: [],
        file,
      };

      acc[name].time.push(time);

      return acc;
    }, {});

  const slowestTestsOverTime = Object.values(allTests)
    .map((test) => {
      const numbers = test.time;

      const med = numbers.sort((a, b) => a.cmp(b))[
        Math.round(test.time.length / 2)
      ];
      const min = Decimal.min(...numbers);
      const max = Decimal.max(...numbers);
      return {
        ...test,
        minAvg: min,
        medAvg: med,
        stdDev: calculateStandardDeviation(numbers),
        maxAvg: max,
        range: max.sub(min),
      };
    })
    .sort((y, x) => x.range.cmp(y.range));

  console.log(
    JSON.stringify({
      pipeline: { web_url: pipeline.web_url, created_at: pipeline.created_at },
      testSuites: testSuites.sort((y, x) => x.average.cmp(y.average)),
      slowestTests: slowestTests
        .sort((y, x) => x.time.cmp(y.time))
        .slice(0, 100),
      overTimeCount: pipelines.length,
      testSuitesOverTime,
      slowestTestsOverTime: slowestTestsOverTime.slice(0, 1000),
      stdDevRange: calculateStandardDeviation(
        slowestTestsOverTime.map((x) => x.range)
      ),
    })
  );
  console.warn(`slowestTestsOverTime ${slowestTestsOverTime.length}`);
  console.warn(`testSuitesOverTime ${testSuitesOverTime.length}`);
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
