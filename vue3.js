#!/usr/bin/env node
import fs from "node:fs";
import path from "node:path";
import Decimal from "decimal.js";
import parser from "fast-xml-parser";
import { DateTime } from "luxon";
import { GitLabAPI, GitLabPagedAPIIterator } from "./lib/api.js";

Decimal.prototype.toJSON = function () {
  return this.toDecimalPlaces(3).toString();
};

const rootDir = import.meta.dirname;
const cacheDir = path.join(rootDir, "cache");

fs.mkdirSync(cacheDir, { recursive: true });

const scannerProjectId = 20666273;
const gitlabProjectId = 278964;

async function pullArtifact(job) {
  console.warn(`Get jest junit artifact for '${job.name}': ${job.web_url}`);

  let junit = { testcase: [] };

  try {
    const { data: xml } = await GitLabAPI.get(
      `/projects/${gitlabProjectId}/jobs/${job.id}/artifacts/junit_jest.xml`,
    );
    junit = parseJunitAndExtract(xml);
  } catch (e) {
    console.warn(
      `Get jest junit artifact for '${job.name}': No artifact, cuz ${e.message}`,
    );
  }

  return { ...job, junit };
}

const noJestJobFile = path.join(cacheDir, "no_jest_jobs_vue3");

const jobNameRegex = /^jest(-with-fixtures)? vue3 \d+\/\d+$/;

async function getLastNPipelinesWithJestJobs(n = 20, prevResults = new Map()) {
  const pipelinesIterator = new GitLabPagedAPIIterator(
    GitLabAPI,
    `/projects/${gitlabProjectId}/pipelines`,
    {
      ref: "master",
      source: "schedule",
    },
  );

  let noJestJobs = [];

  try {
    noJestJobs = JSON.parse(fs.readFileSync(noJestJobFile, "utf-8"));
  } catch {}

  const results = [];

  const webURLS = [...prevResults.values()].map((entry) => entry.web_url);

  for await (const pipeline of pipelinesIterator) {
    if (webURLS.includes(pipeline.web_url) && results.length > 0) {
      return results;
    }
    if (results.length >= n) {
      return results;
    }
    if (!["failed", "success"].includes(pipeline.status)) {
      continue;
    }
    if (noJestJobs.includes(pipeline.id)) {
      continue;
    }

    const cacheFile = path.join(cacheDir, `${pipeline.id}_vue3.json`);

    try {
      results.push(JSON.parse(fs.readFileSync(cacheFile, "utf-8")));
      console.warn("read from cache");
      continue;
    } catch {}

    const jobIterator = new GitLabPagedAPIIterator(
      GitLabAPI,
      `/projects/${gitlabProjectId}/pipelines/${pipeline.id}/jobs`,
    );

    console.warn(`Got data for pipeline: ${pipeline.id}`);

    const jestJobs = [];

    for await (const job of jobIterator) {
      if (!["failed", "success"].includes(job.status)) {
        continue;
      }
      if (jobNameRegex.test(job.name)) {
        jestJobs.push(job);
      }
    }
    if (jestJobs.length) {
      console.warn(`Success! Found some jest jobs for ${pipeline.id} `);
      const res = {
        pipeline,
        jestJobs: await Promise.all(jestJobs.map((job) => pullArtifact(job))),
      };
      results.push(res);
      try {
        fs.writeFileSync(cacheFile, JSON.stringify(res), "utf-8");
      } catch {}
    } else {
      noJestJobs.push(pipeline.id);
      try {
        fs.writeFileSync(noJestJobFile, JSON.stringify(noJestJobs), "utf-8");
      } catch {}
    }
  }
}

function parseJunitAndExtract(xml) {
  const obj = parser.parse(xml, {
    arrayMode: true,
    attributeNamePrefix: "",
    parseNodeValue: false,
    ignoreAttributes: false,
  });
  const suite = obj.testsuites[0].testsuite;

  suite.testcase = flatAndMapCases(suite.testcase);
  return suite;
}

function flatAndMapCases(testcases) {
  if (!testcases) {
    return [];
  }
  if (!Array.isArray(testcases)) {
    return {
      name: testcases.name,
      time: testcases.time,
      file: testcases.file,
    };
  }
  return testcases.flatMap((x) => flatAndMapCases(x));
}

function getTests(jestJobs) {
  return jestJobs
    .flatMap((job) => job.junit)
    .flatMap((suite) => suite.testcase)
    .map((test) => {
      test.time = new Decimal(test.time);
      return test;
    });
}

async function getPrevResults() {
  const pipelinesIterator = new GitLabPagedAPIIterator(
    GitLabAPI,
    `/projects/${scannerProjectId}/jobs`,
    {
      scope: "success",
    },
  );

  let prevResults = new Map();

  for await (const job of pipelinesIterator) {
    // https://gitlab.com/gitlab-org/frontend/playground/jest-speed-reporter/-/merge_requests/12
    // merge date
    if (job.created_at < "2024-05-16T13:58:39.935Z") {
      break;
    }
    if (job.ref !== "main") {
      continue;
    }
    if (job.name !== "vue3") {
      continue;
    }
    if (
      job.artifacts_expire_at &&
      DateTime.fromISO(job.artifacts_expire_at) < DateTime.utc()
    ) {
      break;
    }

    try {
      const { data } = await GitLabAPI.get(
        `/projects/${scannerProjectId}/jobs/${job.id}/artifacts/public/vue3.json`,
      );
      for (const entry of data.history ?? []) {
        prevResults.set(entry.web_url, entry);
      }
    } catch (e) {
      console.warn(
        `Get jest junit artifact for '${job.name}': No artifact, cuz ${e.message}`,
      );
    }
  }

  return prevResults;
}

async function main() {
  const prevResults = await getPrevResults();

  const pipelines = await getLastNPipelinesWithJestJobs(20, prevResults);

  const pipelinesParsed = pipelines.map(({ jestJobs, ...rest }) => {
    const tests = getTests(jestJobs);

    const failedFiles = new Set();
    const allFiles = new Set();

    const failedTests = tests.flatMap((testcase) => {
      const { failure } = testcase;
      allFiles.add(testcase.file);
      if (!failure) {
        return [];
      }
      failedFiles.add(testcase.file);
      return {
        ...testcase,
        failure: Array.isArray(failure)
          ? failure.join("\n- - - - - - -\n")
          : failure,
      };
    });

    return {
      ...rest,
      jestJobsCount:
        jestJobs.filter((job) => job?.junit?.length).length +
        " / " +
        jestJobs.length,
      failedTests,
      failedFiles: failedFiles,
      totalTestCount: tests.length,
      allFilesCount: allFiles.size,
    };
  });

  for (const entry of pipelinesParsed) {
    const {
      pipeline,
      jestJobsCount,
      allFilesCount,
      totalTestCount,
      failedFiles,
      failedTests,
    } = entry;
    const { web_url, created_at, id } = pipeline;
    prevResults.set(web_url, {
      web_url,
      created_at,
      id,
      jestJobsCount,
      failedTestCount: failedTests.length,
      totalTestCount,
      failedFilesCount: failedFiles.size,
      allFilesCount,
    });
  }

  const { pipeline, failedTests, totalTestCount, allFilesCount, failedFiles } =
    pipelinesParsed[0];

  console.log(
    JSON.stringify({
      pipeline: { web_url: pipeline.web_url, created_at: pipeline.created_at },
      failedTests: failedTests,
      totalTestCount: totalTestCount,
      allFilesCount,
      history: [...prevResults.values()].sort((a, b) => {
        if (a.created_at === b.created_at) return 0;
        return a.created_at > b.created_at ? 1 : -1;
      }),
      failedFiles: Array.from(failedFiles),
      overTimeCount: pipelines.length,
    }),
  );
  // console.warn(`slowestTestsOverTime ${slowestTestsOverTime.length}`);
  // console.warn(`testSuitesOverTime ${testSuitesOverTime.length}`);
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
