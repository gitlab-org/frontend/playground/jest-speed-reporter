import { config } from "dotenv";
config();

import axios from "axios";
import {
  setupGitLabAPI,
  GitLabPagedAPIIterator,
} from "gitlab-api-async-iterator";

export const GitLabAPI = setupGitLabAPI(axios, {
  //default values
  baseURL: "https://gitlab.com/api/v4/",
});

export {
  GitLabPagedAPIIterator
}
