#!/bin/bash
# Unofficial bash mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

yarn install --frozen-lockfile
mkdir -p public/

node ./vue3.js | tee public/vue3.json |  jq '.historyJSON = (.history | tostring)' | node_modules/.bin/mustache - vue3.mustache > public/vue3.html

echo "Successfully ran our build."
